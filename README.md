UoA-CS302-2018-P2-ktam069

Python Project - Chat Client


With Python 2.7, CherryPy 3.7.0 and PyCrypto 2.6.1 installed, run in command prompt the following line (while in the project folder):
python Project.py

Open the following URL to access the chat client:
http://localhost:10010/

===========================

Notes:

The fetching of the online users list updates the IP and port of users. It is called automatically every 20 seconds while on the index page.
If staying on a page other than the index for a prolonged period, it may be helpful to return to the index before using other functions to ensure the list is up to date.
(If running the program for the first time without a database, the database should create itself.)

-------

Nodes that Support Features:

Minimum requirements - (kli283, vli121, iryu815, ylee778)

Retrieve profiles for other users, provide profiles to other users - anyone (kli283, vli121, iryu815)
Displays confirmation of message receipt - anyone (kli283, vli121, iryu815)

Read receipts - acknowledge (iryu815, rpor545, jkin677, cril464, dcho415, dche192)
Rate Limiting - anyone (kli283, vli121)
Use of effective inter-app encryption/hashing/data security including handshake - partial implementation (iryu815, dlee906)
Group conversations - anyone (limited functionality - treated as normal messages)

High standard encryption and hashing while maintaining user experience - partial implementation (iryu815, dlee906)
Delete/Clear messages on both sender and receiver side - (iryu815, dlee906)
Fails graciously when interacting with substandard clients - anyone (kli283, vli121, iryu815, ylee778)
Defence against injection attacks - anyone (kli283, iryu815, ylee778, rpor545)
