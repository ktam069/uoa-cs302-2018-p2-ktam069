#!/usr/bin/env python
# -*- coding: utf-8 -*-

listen_ip = "0.0.0.0"
listen_port = 10010

import cherrypy
import os
import sys

import json
from urllib import urlencode
from urllib2 import urlopen
from urllib2 import Request
from urllib2 import URLError
from hashlib import sha256, sha512

import sqlite3
import socket
import time
import thread
import mimetypes
import base64

import DB
import EncryptData
from RateLimitError import RateLimitError

enablePrints = True

class MainApp(object):
	
	initDB = True				# initializes when listUsers is called, if necessary
	
	enLoginThread_dict = {}		# keeps track of whether users are still logged in - used for threading
	
	# CherryPy Configuration
	_cp_config = {
			'tools.encode.on': True, 
			'tools.encode.encoding': 'utf-8',
			'tools.sessions.on': 'True',
			'tools.sessions.timeout': 60,
		}
	
	def __init__(self):
		cherrypy.engine.subscribe("stop", self.endSession)		# set cherrypy to call endSession when it terminates
		self.initDatabases()						# create and initialise database if needed
		self.beginErrorLogFile()					# mark the start of the error log file
		# DB.vacuumDatabase()						# run to defragment the database
	
	@cherrypy.expose
	def default(self, *args, **kwargs):					# default page to redirect to if the url is not recognised
		page = "404 Error - Unrecognised URL"
		cherrypy.response.status = 404
		page += """<br><br><form action='returnToIndex' method='post'>
			<input type='submit' value='Return to Index'></form>"""
		return page
	
	# Index Page
	
	@cherrypy.expose
	def index(self):
		page = ""
		try:
			if (self.isLoggedIn()):				# checks if user is logged in; go to the else clause otherwise
				page += self.mainPage()
			else:								# user is not logged in
				page += "<b>Welcome to the chat client.</b><br>"
				page += "<a href='loginPage'>Click here to log in.</a>"
		except cherrypy.HTTPRedirect:			# if HTTPRedirect was raised, then simply redirect as desired
			raise cherrypy.HTTPRedirect("/")
		except:							# should not reach this code (but just in case)
			page += "Error loading page...something seems to be wrong."
			page += """<form action='returnToIndex' method='post'>
					<input type='submit' value='Retry'></form>"""
			
			self.printAndWriteError("Error loading index page...something seems to be wrong.")
			
		return page
	
	def mainPage(self):							# layout and content display for the index page
		# display information in two columns - the left being functions, and the right being online users
		# (the page is set to refresh every 20 seconds)
		html_css = """<DOCTYPE html>
		<html>
		<head>
			<meta http-equiv='refresh' content='20'>
		<style>
		*{
		}
		
		.column{
			float: left;
			width: 48%;
			padding: 1%;
		}
		.row{
			content:""
			display: table;
			clear: both;
		}
		div{
			word-wrap: break-word;
		}
		</style>
		</head>
		"""
		
		html = """<div class='row'>
					<div class='column' id='col1' name='col1'>"""
		html += self.mainPageFunctions()			# functions on main page (index)
		html += """	</div>
					<div class='column' id='col2' name='col2'>"""
		html += self.mainPageUsers()				# show online users in the second column
		html += """	</div>		
				</div>
				</html>"""
		
		return html_css + html
	
	def mainPageFunctions(self):			# displays the list of functions that the chat client provides
		page  = "<b>Welcome to the chat client, " + self.getUsername() + ".</b><br><br>"
		page += "<a href='listUsers'>List registered users</a><br>"
		page += "<a href='getList'>List online users</a><br>"
		page += "<a href='pingPage'>Ping an online user</a><br><br>"
		page += "<a href='msgPage'>Message a user</a><br>"
		page += "<a href='listMsgs'>Show all messages</a><br>"
		page += "<a href='listUserMsgs'>Show messages for a user</a><br>"
		page += "<a href='clearMessagesPage'>Clear message history</a><br><br>"
		page += "<a href='editProfilePage'>Update your profile</a><br>"
		page += "<a href='requestProfilePage'>Request user profile</a><br>"
		page += "<a href='requestAllProfiles'>Request all user profiles</a><br>"
		page += "<a href='displayAllProfiles'>View all user profiles</a><br><br>"
		page += "<a href='filePage'>Send a file</a><br>"
		page += "<a href='listFiles'>Show received files</a><br>"
		page += "<a href='clearFilesPage'>Clear file history</a><br><br>"
		page += "<a href='setGroupPage'>Create a group</a><br>"
		page += "<a href='getGroupPage'>Get existing group info</a><br>"
		page += "<a href='groupMessagePage'>Message a group</a><br><br>"
		page += """<br><form action='logout' method='post'><input type='submit' value='Log out'></form>"""
		page += "<br>" + self.generateUserProfileHTML()			# adds the current user's profile to the page
		
		return page
	
	def mainPageUsers(self):			# show the list of online users
		page = "<i>Online Users:</i></br>"
		
		html = self.generateGetListHTML()
		
		if (html == ""):
			page += "<br>Could not fetch the online users list.<br>(Please check your connection.)<br>"
		else:
			page += html + "<br>"
		return page
	
	@cherrypy.expose
	def returnToIndex(self):					# redirects to index page
		raise cherrypy.HTTPRedirect('/')
	
	@cherrypy.expose
	def shutdown(self):					# provides the functionality to shutdown the program - mainly for testing purposes
		self.endErrorLogFile()
		cherrypy.engine.exit()
		return "Session ended via shutdown."
	
	@cherrypy.expose
	def endSession(self):		# automatically logs out all users when the application is ended via the terminal
		try:
			if not DB.hasLogins(): return			# execute the rest only if there is a user logged in
			
			logout_result = self.generateQuickLogoutHTML()		# attempts to log out the user
			if (logout_result.count("0") >= 1):
				print "Logged out and shut down."
			else:
				print "Log out unsuccessful."
			
			self.endErrorLogFile()					# mark the end of the error log
		except:
			self.printAndWriteError("Failed to auto log out:")
	
	# Logging In
	
	@cherrypy.expose
	def loginPage(self):
		page = """<form action='login' method='post'>
			Username: <input type="text" name='username'><br>
			Password: <input type="password" name='password'><br>
			<input type='submit' value='Submit'>
			</form>"""
		return page

	@cherrypy.expose
	def login(self, username=None, password=None):			# uses the entered username and password to log in the user
		page = ""
		html = self.generateLoginHTML(username, password)
		
		if (html == ""):
			page += "Invalid Inputs<br>"
		else:
			page += "Response from Login Server:<br>"
			page += html + "<br><br>"
		
		page += """<form action='returnToIndex' method='post'>
			<input type='submit' value='Return to Index'>
			</form>"""
		return page
	
	def generateLoginHTML(self, username, password):			# attempts to log in with the login server and returns the result
		try:
			hashedPassword = sha256((password+username).encode()).hexdigest()		# hashed password in the form required by the login server
			local_ip = socket.gethostbyname(socket.gethostname())
			
			key_dict = EncryptData.RSAGenerateKeys()
			pubKey = key_dict["public"]
			privKey = key_dict["private"]
			
			url = "http://cs302.pythonanywhere.com/report"
			params_dict = {"username": username, "password": hashedPassword,
					"location": "2", "ip": local_ip, "port": listen_port, "pubkey": pubKey, "enc": "1"}
			EncryptData.AESEncryptDict(params_dict)					# encrypt all parameters that aren't enc
			
			params = urlencode(params_dict)
			html = self.fetchURL(url, params)
			
			if (html.count("0") == 1):
				self.setLogin(username, hashedPassword, pubKey, privKey)
				print "\n====================== login info stored ======================\n"
				
			return html
		except:
			self.printAndWriteError("Failed to log in")
			return ""
	
	# Logging Out
	
	@cherrypy.expose
	def logout(self, username=None, password=None):		# attempts to log out the user using info stored in the database
		page = ""
		html = self.generateLogoutHTML()
		
		if (html == ""):
			page += "Logout unsuccessful.<br>"
		else:
			page += "Response from Login Server:<br>"
			page += html + "<br><br>"
		
		page += """<form action='returnToIndex' method='post'>
			<input type='submit' value='Return to Index'></form>"""
		return page
	
	def generateLogoutHTML(self, username=None, password=None):			# gets the user info from the database and attempts to log out
		try:
			username = self.getUsername()
			hashedPassword = self.getHashedPass()

			url = "http://cs302.pythonanywhere.com/logoff"
			url_dict = {"username": username, "password": hashedPassword, "enc": "1"}
			EncryptData.AESEncryptDict(url_dict)				# encrypt all parameters that aren't 'enc'
			params = urlencode(url_dict)
			html = self.fetchURL(url, params)
			
			if (html.count("0") == 1):
				self.stopLoginThread(username)
				self.removeLogin(username)
				print "\n====================== logged out ======================\n"
				
			return html
		except:
			self.printAndWriteError("Failed to log out")
			return ""
	
	@cherrypy.expose
	def generateQuickLogoutHTML(self):			# sends a log out request to the login server for all logged in users
		try:
			for userLogin in DB.getAllLogins():
				username = userLogin[1]
				hashedPassword = userLogin[2]

				url = "http://cs302.pythonanywhere.com/logoff"
				url_dict = {"username": username, "password": hashedPassword, "enc": "1"}
				EncryptData.AESEncryptDict(url_dict)				# encrypt all parameters that aren't 'enc'
				params = urlencode(url_dict)
				html = self.fetchURL(url, params)
				
				self.stopLoginThread(username)		# stop the login threads as well
			
			self.clearLogins()				# remove the logins from the database
			return html
		except:
			self.printAndWriteError("Failed to quick log out")
			return ""
	
	# Management of login details for multiple users
	
	def setLogin(self, username, hashedPass, pubKey=None, privKey=None):				# records login data for the user's session
		cherrypy.session["username"] = username		# stores the username in the browser session
		DB.addLogin(username, hashedPass, pubKey, privKey)			# adds the login data to the database
		self.startLoginThread()						# begin a login thread for this user
	
	def getLogin(self):					# gets the login details from the database
		try:
			targetUser = cherrypy.session.get("username")
			if targetUser == None:		# user is not logged in
				raise ValueError
			
			data_tuple = DB.getLoginData(targetUser)
			if data_tuple == None:		# no user login details retreived
				raise ValueError
			
			dict_data = {}
			dict_data["username"] = data_tuple[1]
			dict_data["hashedPass"] = data_tuple[2]
			dict_data["pubKey"] = data_tuple[3]
			dict_data["privKey"] = data_tuple[4]
			
			return dict_data
		except ValueError:
			self.printAndWriteError("Error: User is not logged in.")
			return {}
		except:
			self.printAndWriteError("Error while retrieving user info.")
			return {}
	
	def removeLogin(self, username):			# removes a specif user's logins
		DB.deleteUesrLogin(username)
		cherrypy.session.clear()
	
	def clearLogins(self):						# clear the database when the session is ended in the terminal
		DB.deleteLogins()
	
	def getUsername(self):					# returns the current user's username
		return self.getLogin().get("username", "")
	
	def getHashedPass(self):				# returns the current user's hashed password
		return self.getLogin().get("hashedPass", "")
	
	def getPubKey(self):					# returns the current user's public key
		return self.getLogin().get("pubKey", "")
	
	def getPrivKey(self):					# returns the current user's private key
		return self.getLogin().get("privKey", "")
	
	def isLoggedIn(self):						# checks if current user is logged in
		targetUser = cherrypy.session.get("username")
		return not (targetUser == None)
	
	# List Registered Users
	
	@cherrypy.expose
	def listUsers(self):					# gets the list of all users and initialise the database if it does not exist
		page = ""
		html = self.generateListUsersHTML()		# gets and displays the list of all registered users from the login server
		
		if (html == ""):
			page += "List users unsuccessful.<br>"
		else:
			page += "Response from Login Server:<br>"
			page += html + "<br><br>"
		
		page += """<form action='returnToIndex' method='post'>
			<input type='submit' value='Return to Index'>
			</form>"""
		return page
	
	def generateListUsersHTML(self):		# sends the list users request to the login server
		try:
			url = "http://cs302.pythonanywhere.com/" + "listUsers"
			html = self.fetchURL(url)
			
			return self.formatCSV(html)
		except:
			self.printAndWriteError("Failed to get users list")
			return ""
	
	def fetchURL(self, url, params=None, timeout=3):		# attempts to open a url (optional url params and timeout settings)
		response = urlopen(url, params, timeout)
		html = response.read()
		return html
	
	def formatCSV(self, string):		# turns comma separated data into html content for display
		string = string.replace(",", "<br>")
		return string
	
	def CSVToList(self, string):		# turns comma separated data into a list
		return string.split(",")
	
	def initDatabases(self):			# init database using the results of listUsers (the list of all users)
		try:
			url = "http://cs302.pythonanywhere.com/listUsers"
			html = self.fetchURL(url)
			
			if self.initDB:				# initialises the database tables if they don't exist
				DB.initLoginDB()
				DB.initUsersDB(self.CSVToList(html))
				DB.initProfilesDB(self.CSVToList(html))
				DB.initOnlineUsersDB()
				DB.initMsgsDB()
				DB.initFilesDB()
				DB.initRequestsDB()
		except:
			self.printAndWriteError("Failed to initialise database with users list")
	
	# List Online Users
	
	@cherrypy.expose
	def getList(self):			# gets the list of online users
		page = ""
		html = self.generateGetListHTML()
		
		if (html == ""):
			page = "List online users unsuccessful.<br>"
		else:
			page = "Response from Login Server:<br>"
			page += html + "<br><br>"
		
		page += """<form action='returnToIndex' method='post'>
			<input type='submit' value='Return to Index'>
			</form>"""
		return page
	
	def generateGetListHTML(self, username=None, password=None):	# gets the online user list as json data
		try:
			username = self.getUsername()
			hashedPassword = self.getHashedPass()
			
			url = "http://cs302.pythonanywhere.com/getList"
			params_dict = {"username": username, "password": hashedPassword, "enc": "1", "json": "1"}
			EncryptData.AESEncryptDict(params_dict)					# encrypt all parameters that aren't enc
			
			params = urlencode(params_dict)
			json_data = self.fetchURL(url, params)		# fetch the url
			dict_data = json.loads(json_data)			# decode json string
			
			DB.updateUsersDB(dict_data)					# update users and onlineUsers tables of the database
			DB.updateOnlineUsersDB(dict_data)
			
			return self.formatDict(dict_data)		# turns the dictionary into a format suitable for html display
		except:
			self.printAndWriteError("Failed to get online users list")
			return ""
	
	# JSON Handling
	
	def fetchJSONURL(self, url, json_params, timeout=3, printURL=True):		# fetch a JSON url and returns the response
		if enablePrints and printURL: print "requesting:", url, "-", json_params 
		request = Request(url, json_params, {"Content-Type": "application/json"})
		response = urlopen(request, None, timeout)
		response = response.read()
		return response
	
	def encodeJSON(self, dict_data):			# encode a dictionary into JSON
		json_data = json.dumps(dict_data)
		return json_data
	
	def decodeJSON(self, json_data):			# decode JSON into a dictionary, and does some checks for html injection
		dict_data = json.loads(json_data)
		self.replaceDictNulls(dict_data)
		self.removeSpecialChars(dict_data)
		return dict_data
	
	def formatDict(self, dict_data):		# turns the dictionary into a format suitable for html display
		html = ""
		for user in dict_data.keys():
			html += "<br>"
			for key,val in dict_data[user].items():
				if key not in {"username", "ip", "location", "lastLogin", "port"}: continue
				if key == "lastLogin": val = val + " <i>(Time: " + self.formatTime(int(val)) + ")</i>"
				html += "<b>" + key + "</b>: " + val + "<br>"
		return html
	
	# Group Setup (Partial Implementation)
	
	@cherrypy.expose
	def setGroupPage(self):					# html for creating a group
		page = """<form action='callSetGroup' method='post'>
			Group Members: <input type="text" name='groupMembers'><br>
			<i>(Comma separated list of group members.)</i><br>
			<input type='submit' value='Submit'>
			</form>"""
		return page

	@cherrypy.expose
	def callSetGroup(self, groupMembers):
		page = ""
		html = self.generateSetGroupHTML(groupMembers)
		
		if (html == ""):
			page += "Invalid Inputs<br>"
		else:
			page += "Response from Login Server:<br><br>"
			page += "Group ID: " + html + "<br><br>"
		
		page += """<form action='groupMessagePage' method='post'>
			<input type='submit' value='Message Group'>
			</form>"""
		page += """<form action='returnToIndex' method='post'>
			<input type='submit' value='Return to Index'>
			</form>"""
		return page
	
	def generateSetGroupHTML(self, groupMembers):		# attempts to set the group and returns result
		try:
			username = self.getUsername()
			hashedPassword = self.getHashedPass()
			
			url = "http://cs302.pythonanywhere.com/setGroup"
			params = urlencode({"username": username, "password": hashedPassword, "groupMembers": groupMembers, "enc": "0"})
			html = self.fetchURL(url, params)
			
			groupID = html.split(", ")[1]
			hasSentID = self.sendGroupID(groupMembers, "groupID: "+groupID)
			
			if (html[0] == "0"):
				returnStr = groupID + "<br><br>"
				if hasSentID:
					returnStr += "Automatically sent group ID to all members."
				else:
					returnStr += "Failed to automatically send group ID to all members. Please do so manually."
					self.printAndWriteError("Failed to automatically send group ID to all members", printStrOnly=True)
				return returnStr
			else:
				return ""
		except:
			self.printAndWriteError("Failed to set group")
			return ""
	
	def sendGroupID(self, groupMembers, groupID):
		successful = True
		members = groupMembers.split(", ")
		
		for user in members:			# send the group ID to all members in the group
			result = self.generateMsgHTML(user, groupID)
			if result.count("success") < 1:
				successful = False
		
		return successful
	
	@cherrypy.expose
	def groupMessagePage(self):					# send a message to all members of a group
		page = """<form action='MsgGroup' method='post'>
			groupID:<br><input type="text" name='groupID'><br>
			Message to send to group:<br><input type="text" name='message'><br>
			<input type='submit' value='Submit'>
			</form>"""
		return page

	@cherrypy.expose
	def MsgGroup(self, groupID, message):
		page = ""
		html = self.generateGroupMsgHTML(groupID, message)
		
		if (html == ""):
			page += "Invalid Inputs<br>"
		else:
			page += "Result:<br><br>"
			page += html + "<br><br>"
		
		page += """<form action='returnToIndex' method='post'>
			<input type='submit' value='Return to Index'>
			</form>"""
		return page
	
	def generateGroupMsgHTML(self, groupID, message):		# attempts to send the entered message to the specified group
		try:
			successful = True
			membersList = self.getMembersFromID(groupID)
			
			for member in membersList:
				print member, message
				result = self.generateMsgHTML(member, message)
				if result.count("success") < 1:
					successful = False
			
			return "Sent messages to all members" if successful else "Failed to send to all members"
		except:
			self.printAndWriteError("Failed to send message group")
			return ""
	
	def getMembersFromID(self, groupID):			# gets the list of users from the login server, from group ID
		try:
			username = self.getUsername()
			hashedPassword = self.getHashedPass()
			
			url = "http://cs302.pythonanywhere.com/getGroup"
			params = urlencode({"username": username, "password": hashedPassword, "groupID": groupID, "enc": "0"})
			html = self.fetchURL(url, params).rstrip()
			
			if (html[0] == "0"):		# successful
				listResults = html.split(", ")
				groupMembers = html.split(listResults[1]+", ")[1]
				return groupMembers.split(", ")			# returns a list of the members as a list
			else:
				return "error"
		except:
			self.printAndWriteError("Failed to get group info")
			return ""
	
	@cherrypy.expose
	def getGroupPage(self):				# get the group members from the group ID
		page = """<form action='callGetGroup' method='post'>
			Group ID: <input type="text" name='groupID'><br>
			<i>(Received from the login server when the group is formed.)</i><br>
			<input type='submit' value='Submit'>
			</form>"""
		return page

	@cherrypy.expose
	def callGetGroup(self, groupID):
		page = ""
		html = self.generateGetGroupHTML(groupID)
		
		if (html == ""):
			page += "Invalid Inputs<br>"
		else:
			page += "Response from Login Server:<br>"
			page += html + "<br><br>"
		
		page += """<form action='returnToIndex' method='post'>
			<input type='submit' value='Return to Index'>
			</form>"""
		return page
	
	def generateGetGroupHTML(self, groupID):			# request the list of members from the login server
		try:
			username = self.getUsername()
			hashedPassword = self.getHashedPass()
			
			url = "http://cs302.pythonanywhere.com/getGroup"
			params = urlencode({"username": username, "password": hashedPassword, "groupID": groupID, "enc": "0"})
			html = self.fetchURL(url, params)
			
			if (html[0] == "0"):
				listResults = html.split(", ")
				returnStr = "Group ID: "+listResults[1]+"<br>"
				returnStr += "Group Members: "+html.split(listResults[1]+", ")[1]
				return returnStr
			else:
				return "error"
		except:
			self.printAndWriteError("Failed to get group info")
			return ""
	
	# (Peer to Peer APIs)
	
	# Ping
		
	@cherrypy.expose
	def ping(self, sender):			# ping by a user
		print " ++ pinged by " + sender
		self.logRequest(sender)			# logs the ping request for rate limiting
		return "0"
	
	@cherrypy.expose
	def pingPage(self):				# ping a user
		page = """<form action='pingUser' method='post'>
			Username: <input type="text" name='username'><br>
			<input type='submit' value='Submit'>
			</form>
			<a href='pingAll'>Ping all online users</a><br>
			<a href='pingAllSlow'>Ping all online users - slow</a><br>"""
		return page

	@cherrypy.expose
	def pingUser(self, username=None):		# attempts to ping an online user
		page = ""
		html = self.generatePingHTML(username)
		
		if (html == ""):
			page += "Invalid Inputs<br>"
		elif (html == "Timed out"):
			page += "Timed out<br>"
		else:
			page = "Response from User:<br>"
			page += html + "<br><br>"
		
		page += """<form action='returnToIndex' method='post'>
			<input type='submit' value='Return to Index'>
			</form>"""
		return page
	
	@cherrypy.expose
	def pingAll(self):			# attempts to ping all online users (short timeout)
		page = ""
		html = self.generatePingAllHTML()
		
		if (html == ""):
			page += "Ping all failed<br>"
		else:
			page = "Response from User:<br>"
			page += html + "<br><br>"
		
		page += """<form action='returnToIndex' method='post'>
			<input type='submit' value='Return to Index'>
			</form>"""
		return page
	
	@cherrypy.expose
	def pingAllSlow(self):			# attempts to ping all online users (long timeout)
		page = ""
		html = self.generatePingAllHTML(timeout=1)
		
		if (html == ""):
			page += "Ping all failed<br>"
		else:
			page = "Response from User:<br>"
			page += html + "<br><br>"
		
		page += """<form action='returnToIndex' method='post'>
			<input type='submit' value='Return to Index'>
			</form>"""
		return page
	
	def generatePingHTML(self, username):		# send request to ping a user
		try:
			userInfo = DB.findUser(username)

			IP = str(userInfo[1])
			port = str(userInfo[4])
			url = "http://" + IP + ":" + port + "/ping"
			url += "?sender=" + self.getUsername()
			
			html = self.fetchURL(url, timeout=3)
			
			return html
		except URLError, e:
			self.printAndWriteError("Error, timed out while pinging")
			return "Timed out"
		except:
			self.printAndWriteError("Error while pinging")
			return ""
	
	def generatePingAllHTML(self, timeout=0.2):		# send requests to ping all users
		try:
			html = ""
			
			listTuples = DB.getAllOnlineUser()
			
			for row in listTuples:
				userInfo = row
				
				IP = str(userInfo[1])
				port = str(userInfo[4])
				username = str(userInfo[0])
				url = "http://" + IP + ":" + port + "/ping"
				url += "?sender=" + str(self.getUsername())
				if enablePrints: print username + " - " + url
				
				html += "<br>"+username+": "+self.fetchPingAllURL(url, timeout=timeout)

			return html
		except:
			self.printAndWriteError("Error while pinging all users")
			return ""

	def fetchPingAllURL(self, url, params=None, timeout=3):		# specialised url fetching to return specific strings based on result
		try:
			response = urlopen(url, params, timeout)
			html = response.read()
			return html
		except URLError, e:
			self.printAndWriteError("No ping response from user")
			return "<i>(No response)</i>"
		except:
			self.printAndWriteError("Error pinging user")
			return "<i>(Error)</i>"
	
	# Hashing and Encoding
	
	def hashData(self, mode, data, username=None):			# hash data based on hashing mode (highest supported is 3)
		if   int(mode) == 0:
			return data
		elif int(mode) == 1:
			return sha256(str(data).encode()).hexdigest()
		elif int(mode) == 2:
			return sha256((str(data)+str(username)).encode()).hexdigest()
		elif int(mode) == 3:
			return sha512((str(data)+str(username)).encode()).hexdigest()
	
	def encodeDict(self, mode, dict):			# encode dictionary data to a standard
		dict_copy = {}
		for key,val in dict.items():
			if   int(mode) == 0:
				dict_copy[key.encode("ascii", "ignore")] = val.encode("ascii", "ignore")
			elif int(mode) == 1:
				dict_copy[key.encode("utf-8")] = val.encode("utf-8")
			elif int(mode) == 2:
				dict_copy[key.encode("utf-16")] = val.encode("utf-16")
			elif int(mode) == 3:
				dict_copy[key.encode("utf-8")] = val.encode("utf-8")
	
	def tryPing(self, username):		# pings a user before sending other requests - throws exceptions if unsuccessful
		userInfo = DB.findUser(username)

		IP = str(userInfo[1])
		port = str(userInfo[4])
		url = "http://" + IP + ":" + port + "/ping"
		url += "?sender=" + self.getUsername()
		
		html = self.fetchURL(url, timeout=0.5)
		
		return html
	
	# Messaging
	
	@cherrypy.expose
	@cherrypy.tools.json_in()
	def receiveMessage(self):				# called when receiving a message
		print " ++ received message"
		try:
			dict_data = cherrypy.request.json		# get JSON data in the form of a dictionary
			self.removeSpecialChars(dict_data)
			if enablePrints: print dict_data
			self.checkRateLimit(dict_data["sender"])	# log request and check if user is rate limited
			DB.updateMsgsDB(dict_data)
			
			return "0"
		except RateLimitError:
			self.printAndWriteError("Ignored receive message request because user exceeded the maximum rate of requests", printStrOnly=True)
			return "11"
		except:
			self.printAndWriteError("Error while receiving message")
			return "1"
	
	@cherrypy.expose
	def msgPage(self):						# page for sending a message to a user
		page = """<form action='msgUser' method='post'>
			Username: <input type="text" name='username'><br>
			Message: <input type="text" name='msg'><br>
			<input type='submit' value='Submit'>
			</form>"""
		return page

	@cherrypy.expose
	def msgUser(self, username=None, msg=None):		# show the result of the message user call
		try:
			page = "Result:<br>"
			result = self.generateMsgHTML(username, msg)
			
			if (result.count("success") >= 1):		# handling the response (or lack thereof) from the user
				page += "Message sent successfully.<br><br>"
			elif (result.count("timed out") >= 1):
				page += "Unable to send message - failed to connect to receiver.<br><br>"
			elif (result.count("error") >= 1):
				page += "Unable to send message - error sending message.<br><br>"
			else:					# either the user responded with an error, or something incorrect (could be extended)
				page += "Unable to send message - receiver responded unexpectedly.<br><br>"
			
			page += """<form action='msgPage' method='post'>
				<input type='submit' value='Send More Messages'>
				</form>
				<form action='returnToIndex' method='post'>
				<input type='submit' value='Return to Index'>
				</form>"""
			
			page += "<i>(The list below do not generate read receipts.)</i><br>"
			page += self.printMsgHistory(tryAcknowledge=False)		# shown for ease of access; does not count as actually viewing the message
			return page
		except:
			self.printAndWriteError("Error while sending message")
			return ""
	
	def generateMsgHTML(self, receiver, msg):		# attempts to message a user
		try:
			sender = self.getUsername()
			msg = msg.replace("<", "_")				# defend against html injection from the user themself
		
			userInfo = DB.findUser(receiver)
			IP = str(userInfo[1])
			port = str(userInfo[4])
		
			url = "http://" + IP + ":" + port + "/receiveMessage"
			
			params = {}
			params["sender"] = sender
			params["destination"] = receiver
			params["message"] = msg
			params["stamp"] = float(self.getCurrentEpoch())
		
			params_json = self.encodeJSON(params)
			self.tryPing(receiver)
			html = self.fetchJSONURL(url, params_json)
						
			if (html.count("0") == 1):
				DB.updateMsgsDB(params)			#add sent message to database
				return "Message sent successfully."
			else:
				self.printAndWriteError("Unable to send message - receiver responded unexpectedly.", printStrOnly=True)
				return "Error response."
		except URLError, e:
			self.printAndWriteError("Error sending message, urlopen timed out")
			return "error, timed out"
		except:
			self.printAndWriteError("Error sending message")
			return "error"
	
	# Printing Messages
	
	@cherrypy.expose
	def listMsgs(self):			# page for showing message history
		page = self.printMsgHistory()
		page += """<br><form action='filteredListMsgs' method='post'>
			Filter by username: <input type="text" name='userFilter' placeholder='(UPI)' style='text-align:center'>
			<input type='submit' value='Filter Messages'>
			</form>"""
		page += """<br><form action='deleteMsgPage' method='post'>
			Delete a message: <input type="text" name='msgID' placeholder='(ID)' style='text-align:center'>
			<input type='submit' value='Delete Message'><br>
			<i>(Please type in the message's ID to delete that message.)</i><br>
			</form>"""
		page += """<br><form action='returnToIndex' method='post'>
			<input type='submit' value='Return to Index'>
			</form>"""
		return page
	
	@cherrypy.expose
	def listUserMsgs(self):			# page for showing message history
		page = self.filteredListMsgs("--Please enter a filter--")
		return page
	
	@cherrypy.expose
	def filteredListMsgs(self, userFilter=""):			# page for showing message history for a specific user
		page = self.printMsgHistory(user2=userFilter)
		page += "<br><i>(filtered for "+userFilter+")</i><br>"
		page += """<br><form action='filteredListMsgs' method='post'>
			Filter by username: <input type="text" name='userFilter' placeholder='(UPI)' style='text-align:center'>
			<input type='submit' value='Filter Messages'>
			</form>"""
		page += """<br><form action='deleteMsgPage' method='post'>
			Delete a message: <input type="text" name='msgID' placeholder='(ID)' style='text-align:center'>
			<input type='submit' value='Delete Message'><br>
			<i>(Please type in the message's ID to delete that message.)</i><br>
			</form>"""
		page += """<br><form action='returnToIndex' method='post'>
			<input type='submit' value='Return to Index'>
			</form>"""
		return page
		
	def printMsgHistory(self, user2=None, tryAcknowledge=True):		# print all messages associated with the current user
		try:
			listTuples = DB.getAllMsgs()		# gets the list of all messages
			
			user1 = self.getUsername()
			if user2=="": user2 = None			# ignore empty filters - treat as no filter
			msgString = self.printUserMsgHistory(listTuples, user1, user2)
			
			if tryAcknowledge:
				self.markMsgsAsRead(user1, user2)
			
			return msgString
		except:
			self.printAndWriteError("Error while getting usernames for printing messages")
			return "Error printing messages..."
	
	def printUserMsgHistory(self, listTuples, filter1=None, filter2=None):
		try:
			msgString = "Message History:<br><br>"
			
			for row in listTuples:		# go through all messages, and append the ones that pass the filter
				sender = str(row[1])
				receiver = str(row[2])
				
				if filter1==None or (sender==filter1 or receiver==filter1):			# display the message if it passes filter 1
					if filter2==None or (sender==filter2 or receiver==filter2):			# and it passes filter 2
						msgString += self.printMsgLine(row, sender, receiver)			# print that message
						
			return msgString
		except:
			self.printAndWriteError("Error while printing messages")
			return "Error printing..."
	
	def printMsgLine(self, row, sender, receiver):		# prints a message as formatted html
		time_epoch = int(float(row[4]))
		time_formatted = self.formatTimeAndDate(time_epoch)
		
		msgString = "<i>"+time_formatted+"</i> - "
		
		# show either '<sender>' or 'to <receiver>'
		if str(self.getUsername()) == "":					# no user is logged in - display the messages anyway, maybe...
			userString = sender
		elif str(row[1]) == str(self.getUsername()):		# if the sender is the logged in user, display receiver
			userString = "to "+receiver
		else:												# otherwise, display the sender
			userString = sender
		
		readStr = str(row[6]) if row[6] != None else "<i>NEW</i>"	# show as read if the message has already been read previously
		acknowStr = str(row[7]) if row[7] != None else "<i>not acknowledged</i>"
		msg = row[3].encode("ascii", "ignore")
		
		msgString += userString+": "+msg+" &nbsp &nbsp ("+readStr+", "+acknowStr+") &nbsp &nbsp &nbsp <i>ID - "+str(row[0])+"</i><br>"
		return msgString
	
	def markMsgsAsRead(self, filter1=None, filter2=None):			# mark an unread message as read and attempt to acknowledge it
		list_tuples = DB.getAllMsgs()
				
		for tuple_data in list_tuples:
			if str(tuple_data[2]) == str(filter1):		# update all the rows where the receiver matches the logged in user
				if filter2==None or (str(tuple_data[2])==filter2 or str(tuple_data[1])==filter2):		# and it passes filter 2
					DB.setMsgRead("read", tuple_data[5])	# mark that message as read
					if (tuple_data[7] == None):				# send an acknowledge request if the message has not been acknowledged
						html = self.generateAcknowledgeHTML(tuple_data)	
						print "acknowledge response:", html
						if (html.count("0") == 1):			# if the other user acknowledged successfully, mark the message as acknowledged
							DB.setMsgAcknowledged("acknowledged", tuple_data[5])
	
	def generateAcknowledgeHTML(self, targetTuple, timeout=0.5):	# attempt to acknowledge a message
		try:
			sender = targetTuple[2]
			stamp = float(targetTuple[4])
			hashing = "1"
			msg = targetTuple[3]
			hash = self.hashData(hashing, msg, sender)
			
			targetUser = targetTuple[1]
			userInfo = DB.findUser(targetUser)
			IP = str(userInfo[1])
			port = str(userInfo[4])
		
			url = "http://" + IP + ":" + port + "/acknowledge"
			
			params_dict = {}
			params_dict["sender"] = sender
			params_dict["stamp"] = stamp
			params_dict["hashing"] = int(hashing)
			params_dict["hash"] = hash
			params_dict["destination"] = targetUser
			
			print "params: ", params_dict
			
			params_json = self.encodeJSON(params_dict)
			self.tryPing(targetUser)
			html = self.fetchJSONURL(url, params_json, timeout)
			
			return html
		except:
			self.printAndWriteError("Error sending acknowledge message request")
			return ""
	
	@cherrypy.expose
	def clearMessagesPage(self):		# delete all messages if yes is selected in the confirmation
		page = "Are you sure you want to clear your message history?<br><br>"
		page += """<form action='clearMessages' method='post'>
			<input type='submit' value='Yes'>
			</form><form action='returnToIndex' method='post'>
			<input type='submit' value='No'>
			</form>"""
		return page

	@cherrypy.expose
	def clearMessages(self):			# delete messages and return to the main page
		DB.deleteAllMsgs()
		self.returnToIndex()
	
	# Time and Time Formatting
	
	def getCurrentEpoch(self):			# return current time in epoch as an integer
		return int(time.time())
	
	def formatTime(self, time_epoch, format=0):		# format the time into a human readable string
		format_string = "%H:%M" if format==0 else "%I:%M %p"		# show AM / PM if format is not 0
		time_struct = time.localtime(time_epoch)
		time_formatted = time.strftime(format_string, time_struct)
		return time_formatted
	
	def formatTimeAndDate(self, time_epoch, format=0):		# format the time and date into a human readable string
		format_string = "%d-%m-%y %H:%M" if format==0 else "%d-%m-%y %I:%M %p"		# show AM / PM if format is not 0
		time_struct = time.localtime(time_epoch)
		time_formatted = time.strftime(format_string, time_struct)
		return time_formatted
	
	# Acknowledge - Read Receipts
	
	@cherrypy.expose
	@cherrypy.tools.json_in()
	def acknowledge(self):				# acknowledging a message, marking the message as acknowledged and read if successful
		print " ++ acknowledge message request"
		try:
			target_dict = cherrypy.request.json
			if enablePrints: print target_dict
			self.checkRateLimit(target_dict["sender"])
			self.removeSpecialChars(target_dict)
			
			if int(target_dict["hashing"]) > 3:			# hashing above 3 not supported
				return "10"
			elif self.checkMsgHash(target_dict):		# checks hash and if matched, mark the message as acknowledged and read
				return "0"
			else:
				return "7"			# hash does not match
		except URLError, e:
			self.printAndWriteError("Error acknowledging message, urlopen timed out")
			return "5"
		except RateLimitError:
			self.printAndWriteError("Ignored acknowledge request because user exceeded the maximum rate of requests", printStrOnly=True)
			return "11"
		except:
			self.printAndWriteError("Error while attempting to acknowledge message")
			return "1"
	
	def checkMsgHash(self, target_dict, deleteMsg=False):		# checks hash and if matched, mark the message as acknowledged and read
		targetReceiver = target_dict["sender"]
		targetStamp = target_dict["stamp"]
		targetHash = target_dict["hash"]
				
		listTuples = DB.getAllMsgs()
		
		msgFound = False
		
		for row in listTuples:
			sentMsgMatches = str(row[2]) == str(targetReceiver) and int(float(row[4])) == int(float((targetStamp)))		# condition for read receipts
			receivedMsgMatches = deleteMsg and str(row[1]) == str(targetReceiver)		# extra condition for deleting messages only
			
			if sentMsgMatches or receivedMsgMatches:	# username and timestamp match
				dbMsg = row[3]
				dbMsgHash = self.hashData(target_dict["hashing"], dbMsg, target_dict["sender"])		# regenerate the hash stored in the database
				
				if dbMsgHash == targetHash:				# found the matching message
					msgFound = True
					self.setMsgReadOrDelete(row[5], deleteMsg)
					
		return msgFound
	
	def setMsgReadOrDelete(self, hash, deleteMsg):
		if deleteMsg:			# if deleting message, then delete the message
			DB.deleteAMsg(hash)							# delete the message
			print "Message deleted"
		else:					# if not deleting, then simply mark as read and acknowledge (by recipient)
			DB.setMsgAcknowledged("acknowledged", hash)	# set message as acknowledge
			DB.setMsgRead("read", hash)					# set message as read
	
	# Encryption Related APIs
	
	@cherrypy.expose
	@cherrypy.tools.json_in()
	def getPublicKey(self):								# public key requested for a user on this server
		print " ++ public key requested"
		try:
			dict_data = cherrypy.request.json				# get JSON data in the form of a dictionary
			if enablePrints: print dict_data
			self.checkRateLimit(dict_data["sender"])	# log request and check if user is rate limited
			pubKey = self.getUserPubKey(dict_data["username"])
			return self.encodeJSON({"error": "0", "pubkey": pubKey})
		except ValueError:
			self.printAndWriteError("Error returning public key: Requested user is not logged in on this server.")
			return "3"
		except:
			self.printAndWriteError("Error returning public key")
			return "1"
	
	def getUserKeys(self, targetUser):			# gets the keys of the requested user from the database
		data_tuple = DB.getLoginData(targetUser)
		if data_tuple == None:		# no user login details retreived - targetUser is not logged into the server
			raise ValueError
		
		dict_data = {}
		dict_data["pubKey"] = data_tuple[3]
		dict_data["privKey"] = data_tuple[4]
		
		return dict_data
	
	def getUserPubKey(self, targetUser):					# gets the specified user's public key
		key_dict = self.getUserKeys(targetUser)
		return key_dict.get("pubKey", "")
	
	def getUserPrivKey(self, targetUser):					# gets the the specified user's private key
		key_dict = self.getUserKeys(targetUser)
		return key_dict.get("privKey", "")
	
	@cherrypy.expose
	def requestPublicKey(self, destination):			# request another user's public key
		try:
			username = self.getUsername()
			userInfo = DB.findUser(destination)
			IP = str(userInfo[1])
			port = str(userInfo[4])
		
			url = "http://" + IP + ":" + port + "/getPublicKey"
			
			params_dict = {}
			params_dict["sender"] = username
			params_dict["username"] = destination
						
			params_json = self.encodeJSON(params_dict)
			self.tryPing(destination)
			html = self.fetchJSONURL(url, params_json)
			
			return html
		except:
			self.printAndWriteError("Error getting public key from user")
			return ""
	
	@cherrypy.expose
	@cherrypy.tools.json_in()
	def handshake(self):							# attempts to decrypt a message based on the encryption standard
		print " ++ handshake requested"
		try:
			dict_data = cherrypy.request.json				# get JSON data in the form of a dictionary
			# if enablePrints: print dict_data
			self.checkRateLimit(dict_data["sender"])	# log request and check if user is rate limited
			enc_mode = int(dict_data["encryption"])
			if enc_mode == 3:
				privKey = self.getUserPrivKey(dict_data["destination"])
				message = EncryptData.RSADecrypt(str(dict_data["message"]), privKey)
				return self.encodeJSON({"error": "0", "message": message})
			
			return self.encodeJSON({"error": "9", "message": "error"})		# return an error if mode is unsupported
		except:
			self.printAndWriteError("Error during handshake")
			return self.encodeJSON({"error": "1", "message": "error"})
	
	# Acknowledge Delete for Messages
	
	@cherrypy.expose
	@cherrypy.tools.json_in()
	def acknowledgeDelete(self):	# acknowledging the deletion of a message, the other user should only delete the message if this returns 0
		print " ++ acknowledge delete request"
		try:
			target_dict = cherrypy.request.json
			if enablePrints: print target_dict
			self.checkRateLimit(target_dict["sender"])
			self.removeSpecialChars(target_dict)
			
			if int(target_dict["encryption"]) == 3:
				privKey = self.getUserPrivKey(target_dict["destination"])
				target_dict["hash"] = EncryptData.RSADecrypt(target_dict["hash"], privKey)
				target_dict["stamp"] = float( EncryptData.RSADecrypt(target_dict["stamp"], privKey) )
				
				if self.checkMsgHash(target_dict, deleteMsg=True):		# checks hash and if matched, delete message if found
					return "0"
				else:
					return "7"						# hash does not match
			
			return "9"				# encryption not supported
		except URLError, e:
			self.printAndWriteError("Error acknowledging  delete message, urlopen timed out")
			return "5"
		except RateLimitError:
			self.printAndWriteError("Ignored acknowledge delete request because user exceeded the maximum rate of requests", printStrOnly=True)
			return "11"
		except:
			self.printAndWriteError("Error while attempting to acknowledge delete message")
			return "1"
	
	@cherrypy.expose
	def deleteMsgPage(self, msgID):			# page for showing result of deleting message
		page = self.generateAcknowledgeDeleteHTML(msgID)
		if (page.count("0") == 1):		# if the other person acknowledged successfully, then delete the message
			DB.deleteByID(msgID)
		page += """<br><br><form action='filteredListMsgs' method='post'>
			<input type='submit' value='Return to Message Display Page'>
			</form>"""
		page += """<br><form action='returnToIndex' method='post'>
			<input type='submit' value='Return to Index'>
			</form>"""
		return page
	
	def findMsgByID(self, msgID):
		try:
			tuple_data = DB.getMsgByID(msgID)
			return tuple_data
		except:
			return "Could not find the message requested for deletion.<br>"
	
	def generateAcknowledgeDeleteHTML(self, msgID, timeout=1):		# attempt to delete a message
		try:
			targetTuple = self.findMsgByID(msgID)
			if targetTuple==None:
				return "Could not find the message requested for deletion"
			
			username = self.getUsername()			# get the session user's upi
			
			sender = username
			stamp = float(targetTuple[4])
			hashing = "2"
			msg = targetTuple[3]
			hash = self.hashData(hashing, msg, sender)
			
			if not str(username)==str(targetTuple[1]):		# destination is either the person who is not the user
				destination = targetTuple[1]				# ...or the user if it was sent to themself
			else:				# in either case, if one of them matches the user, then the other is the destination
				destination = targetTuple[2]
			
			userInfo = DB.findUser(destination)
			IP = str(userInfo[1])
			port = str(userInfo[4])
		
			url = "http://" + IP + ":" + port + "/acknowledgeDelete"
			
			params_dict = {}
			params_dict["sender"] = sender
			params_dict["stamp"] = stamp
			params_dict["hashing"] = int(hashing)
			params_dict["hash"] = hash
			params_dict["destination"] = destination
			params_dict["encryption"] = 3
					
			pubKey_json = self.requestPublicKey(destination)	# get the other person's public key
			pubKey_dict = self.decodeJSON(pubKey_json)
					
			pubKey = pubKey_dict["pubkey"]
			if not str(pubKey_dict["error"]) == "0":		# raise an error if the key could not be requested
				raise ValueError
			
			params_dict["stamp"] = EncryptData.RSAEncrypt(str(params_dict["stamp"]), pubKey)
			params_dict["hash"] = EncryptData.RSAEncrypt(str(params_dict["hash"]), pubKey)
			
			if enablePrints: print "params: ", params_dict
			
			params_json = self.encodeJSON(params_dict)
			self.tryPing(destination)
			html = self.fetchJSONURL(url, params_json)		# request acknowledgeDelete
			
			return html
		except ValueError, e:
			self.printAndWriteError("Error sending acknowledge delete message request - public key could not be requested")
			return "Error sending acknowledge delete message request - public key could not be requested."
		except URLError, e:
			self.printAndWriteError("Error sending acknowledge delete message request - could not connect to user")
			return "Error sending acknowledge delete message request - could not connect to user."
		except:
			self.printAndWriteError("Error sending acknowledge delete message request")
			return "Error sending acknowledge delete message request."
	
	# User Profiles
	
	@cherrypy.expose
	@cherrypy.tools.json_in()
	def getProfile(self):					# received a request to get a profile
		print " ++ getProfile request" 
		try:
			dict_data = cherrypy.request.json
			if enablePrints: print dict_data
			self.checkRateLimit(dict_data["sender"])
			self.removeSpecialChars(dict_data)
			
			profile_dict = DB.findProfile(dict_data["profile_username"])
			
			if profile_dict == None:					# profile not in database
				return self.encodeJSON({"error": "nothing found"})		# might want to handle this differently (should not occur)
			else:
				del profile_dict["username"]			#username isn't required for the JSON params
				profile_json = self.encodeJSON(profile_dict)
				return profile_json
		except RateLimitError:
			self.printAndWriteError("Ignored get profile request because user exceeded the maximum rate of requests", printStrOnly=True)
			return "11"
		except:
			self.printAndWriteError("Error processing getProfile request")
			return "1"
	
	@cherrypy.expose
	def editProfilePage(self):			# page for editing logged in user's profile
		page = """<form action='updateUserProfile' method='post'>
			fullname: <input type="text" name='fullname' placeholder='(full name)' style='text-align:center'><br>
			position: <input type="text" name='position' placeholder='(job position)' style='text-align:center'><br>
			description: <input type="text" name='description' placeholder='(description)' style='text-align:center'><br>
			location: <input type="text" name='location' placeholder='(physical location)' style='text-align:center'><br>
			picture: <input type="text" name='picture' placeholder='(picture url)' style='text-align:center'><br>
			<input type='submit' value='Submit'>
			</form>"""
		return page
	
	@cherrypy.expose
	def updateUserProfile(self, fullname, position, description, location, picture):	# update stored profile
		page = ""
		try:
			username = self.getUsername()
			lastUpdated = self.getCurrentEpoch()
			
			list_data = [username, lastUpdated, fullname, position, description, location, picture]
			list_data = self.removeSpecialCharsList(list_data)
			DB.updateProfilesDB(tuple(list_data))
			
			page += "Profile updated.<br>"
		except:
			self.printAndWriteError("Cannot update profile as user is not logged in")
			page += "Please log in.<br>"
		
		page += """<form action='returnToIndex' method='post'>
			<input type='submit' value='Return to Index'>
			</form>"""
		return page
	
	@cherrypy.expose
	def requestProfilePage(self):			# get another user's profile
		page = """<form action='requestProfile' method='post'>
			Username of Profile: <input type="text" name='profile_username'><br>
			Username to Request from: <input type="text" name='recipient'><br>
			<i>(Will request the profile from the user's node if the second field is left empty.)</i><br>
			<input type='submit' value='Submit'>
			</form>"""
		return page
		
	@cherrypy.expose
	def requestProfile(self, profile_username, recipient):		# request a user's profile from someone
		if recipient == "" or recipient == None:		# autofill the recipient field with username if it's left empty
			recipient = profile_username
		
		page = ""
		html = self.generateGetProfileHTML(profile_username, recipient)
			
		if (html == ""):
			page += "Unable to get profile<br>"
		else:
			page += "Response from User:<br>"
			page += html + "<br><br>"
		
		page += """<form action='returnToIndex' method='post'>
			<input type='submit' value='Return to Index'>
			</form>"""
		return page
	
	def generateGetProfileHTML(self, profile_username, recipient):		# get a specific user's profile from a node
		try:
			userInfo = DB.findUser(recipient)
			IP = str(userInfo[1])
			port = str(userInfo[4])
		
			url = "http://" + IP + ":" + port + "/getProfile"
			
			params_dict = {}
			params_dict["profile_username"] = profile_username
			params_dict["sender"] = self.getUsername()
			
			params_json = self.encodeJSON(params_dict)
			self.tryPing(recipient)
			html = self.fetchJSONURL(url, params_json)
			html = html.replace("<", "_")
			
			if (html != ""):		# if there was a response, display it and update DB
				print html, " - ", profile_username
				self.checkAndUpdateProfile(html, profile_username)
				html += "<br><br><br>" + self.generateUserProfileHTML(profile_username)
			
			return html
		except:
			self.printAndWriteError("Error getting profile")
			return ""
	
	@cherrypy.expose
	def requestAllProfiles(self):			# request all online users' profiles
		page = ""
		html = self.generateGetAllProfilesHTML(timeout=0.3)
		
		if (html == ""):
			page += "Request all profiles failed<br>"
		else:
			page += "Response from User:<br>"
			page += html + "<br><br>"
		
		page += """<form action='returnToIndex' method='post'>
			<input type='submit' value='Return to Index'>
			</form>"""
		return page
	
	def generateGetAllProfilesHTML(self, timeout=1):
		try:
			html = ""
			list_user_json = {}
			
			listTuples = DB.getAllOnlineUser()
			
			for row in listTuples:		# fetch profiles from the list of online users, storing outputs into a list 
				userInfo = row
				
				IP = str(userInfo[1])
				port = str(userInfo[4])
				username = str(userInfo[0])
				url = "http://" + IP + ":" + port + "/getProfile"
				
				params_dict = {}
				params_dict["profile_username"] = username
				params_dict["sender"] = self.getUsername()
				
				params_json = self.encodeJSON(params_dict)
				user_json = self.fetchGetALLJSONURL(url, params_json, timeout=timeout)
				list_user_json[username] = user_json
				html += "<br>"+username+": "+user_json
			
			for username,user_json in list_user_json.items():		# update the database if there was a valid response
				if (user_json.count("No response") == 0 and user_json.count("Error") == 0):
					# print user_json, " - ", username
					self.checkAndUpdateProfile(user_json, username)
			
			return html
		except:
			self.printAndWriteError("Error getting all profiles")
			return ""
	
	def fetchGetALLJSONURL(self, url, json_params, timeout=3):		# fetch all JSON URLs for getting all profiles
		try:
			html = self.fetchJSONURL(url, json_params)
			
			# try to read the dictionary to see if it's valid
			dict_data = self.decodeJSON(html)
			lastUpdated = dict_data.get("lastUpdated", None)
			if lastUpdated == None or lastUpdated == "":
				raise ValueError
			
			html = html.replace("<", "_")

			if enablePrints: print "Successful"
			return html
		except URLError, e:
			self.printAndWriteError("JSON request timed out (while getting all profiles):")
			return "<i>(No response)</i>"
		except:
			self.printAndWriteError("Invalid response ignored (while getting all profiles):")
			return "<i>(Error)</i>"
	
	def checkAndUpdateProfile(self, user_json, username):		# checks if a user's profile should be updated and do so if it should
		dict_data = self.decodeJSON(user_json)
		
		lastUpdated = dict_data.get("lastUpdated", "0")
		fullname 	= dict_data.get("fullname", "-")
		position 	= dict_data.get("position", "-")
		description = dict_data.get("description", "-")
		location 	= dict_data.get("location", "-")
		picture 	= dict_data.get("picture", "-")
		
		try:			# check if timestamp is valid, if not then don't update database
			lastUpdated = float(lastUpdated)
		except:
			return			# exit and do nothing
		
		dbData = DB.findProfile(username)		# find a user's profile
		if dbData != None and dbData["lastUpdated"] < lastUpdated:		# update only if user doesn't exist or data is more recent
			print "updating db for " + str(username) + ": " + str(user_json)
			list_data = [username, lastUpdated, fullname, position, description, location, picture]
			list_data = self.removeSpecialCharsList(list_data)
			DB.updateProfilesDB(tuple(list_data))
	
	def replaceDictNulls(self, dict):		# replace null dictionary values with empty strings
		for key,val in dict.items():
			if val == None:
				dict[key] = ""
		
	def removeSpecialChars(self, dict):		# replace all open angled brackets with underscores for ignoring html
		for key,val in dict.items():
			if isinstance(val, unicode): val = val.encode("ascii", "ignore")
			dict[key] = str(val).replace("<", "_")
		
	def removeSpecialCharsList(self, list):		# replace all open angled brackets with underscores for ignoring html (for a list)
		list = [str(x).replace("<", "_") for x in list]
		return list
	
	# Displaying Profiles
	
	@cherrypy.expose
	def displayAllProfiles(self):			# display all stored profiles
		page = ""
		html = self.generateDisplayAllProfilesHTML()
		
		if (html == ""):
			page += "Display all profiles failed<br>"
		else:
			page += "Profiles in Database:<br>"
			page += html + "<br><br>"
		
		page += """<form action='returnToIndex' method='post'>
			<input type='submit' value='Return to Index'>
			</form>"""
		return page
	
	def generateDisplayAllProfilesHTML(self):		# append all user profiles into html display
		try:
			html = ""
			
			list_dicts = DB.getAllProfilesDict()
			
			for dict in list_dicts:
				html += self.profileDictToHTML(dict)
			
			return html
		except:
			self.printAndWriteError("Error displaying all profiles")
			return ""
	
	def generateUserProfileHTML(self, username=None):		# generate HTML display for the specified user's profile
		try:
			html = ""
			
			if username == None:
				username = self.getUsername()
				
			profile_dict = DB.findProfile(username)
			
			# if the profile does not exist, raise error and go to except clause
			lastUpdated = profile_dict.get("lastUpdated", None)
			if int(lastUpdated) == 0:
				raise ValueError
			
			html += self.profileDictToHTML(profile_dict, "100")
			
			return html
		except:
			self.printAndWriteError("(No profile found for logged in user)")
			return "<br>(No Profile Found)"
	
	def profileDictToHTML(self, dict, maxSize="200"):		# turn the profile passed in into HTML display code
		html = ""
		
		if int(dict["lastUpdated"]) == 0:		# ignore if the profile has not been retrieved previously
			return ""
		
		username = dict["username"]
		lastUpdated = dict["lastUpdated"]
		fullname = dict["fullname"]
		position = dict["position"]
		description = dict["description"]
		location = dict["location"]
		pictureURL = dict["picture"]
		
		lastUpdated_time = self.formatTimeAndDate(float(lastUpdated))
		
		html += "<br><b>"+username+":</b>"" (Last updated "+lastUpdated_time+")<br>"
		html += "<p style='text-align:left;'>Profile picture:<br> "
		html += "<img src='"+pictureURL+"' alt='(image failed to load)' style='max-height:"+maxSize+"px; max-width:"+maxSize+"px'> </p>"
		html += "Full name: "+fullname+"<br>"
		html += "Position: "+position+"<br>"
		html += "Description: "+description+"<br>"
		html += "Location: "+location+"<br><br>"
		
		return html
	
	# Files Sending and Receiving
		
	@cherrypy.expose
	@cherrypy.tools.json_in()
	def receiveFile(self):			# received a file - attempt to save it and record it in the database
		print " ++ received file" 
		try:
			dict_data = cherrypy.request.json
			# self.removeSpecialChars(dict_data)
			self.checkRateLimit(dict_data["sender"])
			
			self.formatFileName(dict_data)				# modify the filename (replace spaces with underscores
			fileWasSaved = self.saveFile(dict_data)		# ...and save the file
			
			if fileWasSaved:	# files was saved successfully
				DB.updateFilesDB(dict_data)
			else:				# file is too large
				self.printAndWriteError("Ignored receive file request because the file was too large", printStrOnly=True)
				return "12"
		
			return "0"
		except RateLimitError:
			self.printAndWriteError("Ignored receive file request because user exceeded the maximum rate of requests", printStrOnly=True)
			return "11"
		except:
			self.printAndWriteError("Error receiving file")
			return "1"
	
	def saveFile(self, dict_data, sizeLimit=5):			# save the file if the files is less than sizeLimit (in MB, default 5)
		type = dict_data["content_type"]
		timestamp = int(float(dict_data["stamp"]))
		filename = dict_data["filename"]
		file_path = "files/dl/"+filename
		
		file_data = base64.b64decode(dict_data["file"])		# encode the file in base64

		with open(file_path, "wb") as f:			# the with statement ensures that the file closes even if an exception is thrown
			f.write(file_data)					# write the data to a file
		
		if os.path.getsize(file_path) > (sizeLimit*1024*1024):				# do not add to database if file is larger than allowed
			return False
		
		return True
	
	def formatFileName(self, dict_data):			# replaces the filename value of the input dictionary
		timestamp = int(float(dict_data["stamp"]))
		filename = str(timestamp)+"_"+dict_data["filename"]		# prepend filename with the timestamp to deal with duplicates
		filename = filename.replace(" ", "_")					# replace spaces with underscores in the file name for easier handling
		dict_data["filename"] = filename						# modify the existing filename value
	
	@cherrypy.expose
	def filePage(self):			# page for sending file
		page = """<form action='sendFileToUser' method='post' enctype='multipart/form-data'>
			Username: <input type="text" name='username'><br>
			File: <input type="file" name='file'><br>
			<input type='submit' value='Submit'>
			</form>"""
		return page

	@cherrypy.expose
	def sendFileToUser(self, username=None, file=None):		# attempt to send the file and display the result
		try:
			page = ""
			html = self.generateFileHTML(username, file)
			
			if (html == ""):
				page += "Unable to send file - no response<br>"
			else:
				page += "Response from User:<br>"
				page += html + "<br><br>"
			
			page += """<form action='filePage' method='post'>
				<input type='submit' value='Send More Files'>
				</form>
				<form action='returnToIndex' method='post'>
				<input type='submit' value='Return to Index'>
				</form>"""
			
			return page
		except:
			self.printAndWriteError("Error sending file")
			return ""
	
	def generateFileHTML(self, receiver, file):			# generates the request for sending the file to the receiver
		try:
			sender = self.getUsername()
			filename = file.filename.replace(" ", "_")
			
			file_data = file.file.read()					# read and encode the file uploaded
			file_base64 = base64.b64encode(file_data)
		
			userInfo = DB.findUser(receiver)
			IP = str(userInfo[1])
			port = str(userInfo[4])
		
			url = "http://" + IP + ":" + port + "/receiveFile"
		
			params = {}
			params["sender"] = sender
			params["destination"] = receiver
			params["file"] = file_base64
			params["filename"] = filename
			params["content_type"] = mimetypes.guess_type(filename)[0]
			params["stamp"] = int(time.time())
			
			fileWasSaved = self.saveFile(params)		# save the file to be sent
			if fileWasSaved:
				DB.updateFilesDB(params)			# adds the file to the database if it was saved successfully (within size limit)
			else:				# file is too large
				self.printAndWriteError("Ignored file sending because the file is too large", printStrOnly=True)
				return "File did not send as it is too large."
		
			params_json = self.encodeJSON(params)
			self.tryPing(receiver)
			html = self.fetchJSONURL(url, params_json, printURL=False)		# send the file (do not print to terminal)
			
			return html
		except URLError, e:
			self.printAndWriteError("Error sending file, urlopen timed out")
			return ""
		except:
			self.printAndWriteError("Error sending file")
			return ""
	
	# Display Files
	
	@cherrypy.expose
	def listFiles(self):		# show all files received by the current user
		page = self.showFilesHistory()
		page += """<br><form action='returnToIndex' method='post'>
			<input type='submit' value='Return to Index'>
			</form>"""
		return page
		
	def showFilesHistory(self):
		try:
			user1 = self.getUsername()
			return self.printFilesHistory(maxSize="500", filter1=user1)		# retrieve the files
		except:
			self.printAndWriteError("Error loading files to be displayed")
			return "Error showing files..."
	
	def printFilesHistory(self, maxSize="500", filter1=None):
		filesString = "Files Received:<br><br>"
		local_ip = socket.gethostbyname(socket.gethostname())				# generate url for hosting static file
		static_url = "http://"+local_ip+":"+str(listen_port)+"/static/"
		
		listTuples = DB.getAllFiles()
		
		for row in listTuples:			# display each file in the database that was received by the current user (filter1)
			type = row[5]
			fileType = type.split("/")[0]
			timestamp = int(float(row[6]))
			time_formatted = self.formatTimeAndDate(timestamp)

			filename = str(row[4])
			fileURL = static_url +"dl/"+ filename		
			
			sender = str(row[1])
			receiver = str(row[2])
			if filter1==None or (receiver==filter1):			# display the files if it passes filter 1
				filesString += self.embedFile(sender, type, fileType, time_formatted, filename, fileURL, maxSize)
			
		return filesString
	
	def embedFile(self, sender, type, fileType, time_formatted, filename, fileURL, maxSize="500"):		# embed file into html display
		embededStr = ""
		
		if not os.path.exists("files/dl/"+filename):				# if the file was deleted manually by someone, then don't try to embed it
			embededStr += "<i>"+time_formatted+"</i> - "+sender+": <br>"+"("+filename+") <br>"
			embededStr += "(File got deleted locally!) <br><br><br>"
		elif fileType == "image":
			embededStr += "<i>"+time_formatted+"</i> - "+sender+": <br>"+"("+filename+") <br>"
			embededStr += "<br><img src='"+fileURL+"' alt='(image failed to load)' style='max-height:"+maxSize+"px; max-width:"+maxSize+"px'> <br><br><br>"
		elif fileType == "application":
			embededStr += "<i>"+time_formatted+"</i> - "+sender+": <br>"+"("+filename+") <br>"
			embededStr += "<br><embed src='"+fileURL+"' style='height:"+maxSize+"px; width:"+maxSize+"px' type='"+type+"'> <br><br><br>"
		elif fileType == "audio":
			embededStr += "<i>"+time_formatted+"</i> - "+sender+": <br>"+"("+filename+") <br>"
			embededStr += "<br><audio controls play><source src='"+fileURL+"' type='"+type+"'></audio> <br><br><br>"
		elif fileType == "video":
			embededStr += "<i>"+time_formatted+"</i> - "+sender+": <br>"+"("+filename+") <br>"
			embededStr += "<br><video width='"+maxSize+"' controls play><source src='"+fileURL+"' type='"+type+"'></video> <br><br><br>"
		else:
			embededStr += "<i>"+time_formatted+"</i> - "+sender+": <br>"+"("+filename+") <br>"
			# embededStr += "<br><object src='"+fileURL+"' style='max-height:"+maxSize+"px; max-width:"+maxSize+"px' type='"+type+"'> <br><br><br>"
			embededStr += "<br> File could not be loaded - go to files/dl to view the file manually."
		
		return embededStr
	
	@cherrypy.expose
	def clearFilesPage(self):		# delete all files from the database if confirmed
		page = "Are you sure you want to clear your file history?<br><br>"
		page += """<form action='clearFiles' method='post'>
			<input type='submit' value='Yes'>
			</form><form action='returnToIndex' method='post'>
			<input type='submit' value='No'>
			</form>"""
		return page

	@cherrypy.expose
	def clearFiles(self):
		DB.deleteAllFiles()			# delete files from the database, note that the actual files aren't deleted locally
		self.returnToIndex()
	
	# Threading for staying logged in continuously - does not currently support multiple users
	
	def startLoginThread(self):			# create a new login thread for the user currently logged in
		try:
			url = "http://cs302.pythonanywhere.com/report"
			url_dict = self.createContLoginDict()				# creates the login parameters for the logged in user
			params = urlencode(url_dict)
			
			username = url_dict["username"]
			self.enLoginThread_dict[username] = True			# set the boolean for this user to True - thread runs while it remains True
			
			thread.start_new_thread(self.continuousLogin, (url, params, self.enLoginThread_dict[username]))		# start the thread
		except:
			self.printAndWriteError("Login thread error:")
	
	def continuousLogin(self, url, params, runThread):		# run the login thread as long as runThread is True
		while runThread:
			time.sleep(90)						# execute every 1:30 minutes to be on the safe side
			self.runThreadLoginHTML(url, params)
	
	def stopLoginThread(self, username):			# stop the thread by toggling the boolean condition for the running of the thread
		self.enLoginThread_dict[username] = False
	
	def runThreadLoginHTML(self, url, params):		# send the report request to the login server
		try:
			html = self.fetchURL(url, params)
			
			if (html.count("0") == 1):				# re logged in successfully
				print "== reinitiated login =="
		except:
			self.printAndWriteError("Login thread error (while fetching URL): ")
		
	def createContLoginDict(self):		# generate the dictionary to pass to the login server for this user
		username = self.getUsername()
		hashedPassword = self.getHashedPass()
		
		pubKey = self.getPubKey()
		privKey = self.getPrivKey()
		
		local_ip = socket.gethostbyname(socket.gethostname())
		
		url_dict = {"username": username, "password": hashedPassword,
					"location": "2", "ip": local_ip, "port": listen_port, "pubkey": pubKey, "enc": "1"}
					
		EncryptData.AESEncryptDict(url_dict)				# encrypt all parameters that aren't 'enc'
		
		return url_dict
	
	# Error logging
	
	def beginErrorLogFile(self):			# mark the start of the program
		time_formatted = self.getFormattedTimeAndDate()
		self.writeToErrorLog("\n\n=== New session started === ("+time_formatted+")\n")
		
	def endErrorLogFile(self):				# mark the ending of a session
		time_formatted = self.getFormattedTimeAndDate()
		self.writeToErrorLog("(Session was ended - "+time_formatted+")")
	
	def writeToErrorLog(self, text):
		with open("error_log.txt", "a+") as f:			# append any writes to the end of the eror log file
			f.write(text+"\n")
			
	def printAndWriteError(self, extraInfo="", printStrOnly=False):			# write errors to the error log using format chosen
		try:
			time_formatted = self.getFormattedTimeAndDate()
			
			if printStrOnly:		# only print/write the string passed in
				print extraInfo
				self.writeToErrorLog(time_formatted+" - "+extraInfo)
			elif extraInfo != "":		# print/write the string concatinated to the error if it's non-empty
				print extraInfo, sys.exc_info(), "line ", sys.exc_info()[2].tb_lineno
				self.writeToErrorLog(time_formatted+" - "+extraInfo+" "+str(sys.exc_info())+", line "+str(sys.exc_info()[2].tb_lineno))
			else:
				print sys.exc_info(), "line ", sys.exc_info()[2].tb_lineno
				self.writeToErrorLog(time_formatted+" - "+str(sys.exc_info())+", line "+str(sys.exc_info()[2].tb_lineno))
		except:
			print "== Error while writing to error_log.txt =="
			print sys.exc_info(), "line ", sys.exc_info()[2].tb_lineno
	
	def getFormattedTimeAndDate(self):		# get time and date in a readable form
		time_epoch = self.getCurrentEpoch()
		time_formatted = self.formatTimeAndDate(time_epoch)
		return time_formatted
	
	# Rate Limiting
	
	def checkRateLimit(self, username, timestamp=None, limitPerMin=30):
		# checks whether a user has exceeded the rate limit (limitPerMin), raising a RateLimitError if they have
		# and also logs the current request into the database
		count = self.countRequests(username)
		if int(count) > int(limitPerMin):
			raise RateLimitError
		self.logRequest(username)			# do not have to log request if said request was already rate limited
	
	def logRequest(self, username, timestamp=None):		# logs the incoming request based on username and timestamp
		try:
			if timestamp==None:
				timestamp = self.getCurrentEpoch()
			DB.addRequest(username, timestamp)
		except:
			self.printAndWriteError("Failed to log request for rate limiting")
	
	def countRequests(self, username, sinceTimestamp=None):		# counts the number of requests from the user in the last 120 seconds (by default)
		try:
			if sinceTimestamp==None:		# count only requests in the last minute
				sinceTimestamp = self.getCurrentEpoch() - 120
			DB.deleteRequestRange(sinceTimestamp)			# remove all rows with timestamps that are no longer relevant (optional)
			matchedListTuples = DB.getUserRequests(username, sinceTimestamp)	# get the list of tuples with timestamps in the last minute
			if matchedListTuples==None:
				return 0
			else:
				return len(matchedListTuples)
		except:
			self.printAndWriteError("Failed to count requests for rate limiting")
			return 0


def runMainApp():
	# listen for connections on the configured address and port.
	port_conf 	= {	'server.socket_host': listen_ip,
					'server.socket_port': listen_port,
					'engine.autoreload.on': True
				}
	
	# configurations for static file serving
	root_path = os.path.dirname(os.path.abspath(__file__))
	static_conf = {
					"/static": {
						'tools.staticdir.root': root_path,
						'tools.staticdir.on': True,
						'tools.staticdir.dir': './files'
					}
				}
	
	cherrypy.config.update(config=port_conf)						# set cherrypy port config
	cherrypy.tree.mount(MainApp(), "/", config=static_conf)			# mount MainApp and set up static config
	
	print "\n\n============ Program Starting ============\n"
	
	# start cherrypy and block other code
	cherrypy.engine.start()
	cherrypy.engine.block()

# run the program
runMainApp()
