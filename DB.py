import sqlite3
import sys
from hashlib import sha256

def initUsersDB(user_list):
	try:
		conn = sqlite3.connect("database.db")
		
		usersSQL = """CREATE TABLE IF NOT EXISTS users(
						id 			integer PRIMARY KEY,
						username	text	UNIQUE,
						ip			text	NOT NULL,
						location	text	NOT NULL,
						lastLogin	text	NOT NULL,
						port		text	NOT NULL)"""
		conn.execute(usersSQL)
		
		if usersDBIsEmpty():
			insertUserSQL = """INSERT OR REPLACE INTO users (username, ip, location, lastLogin, port) VALUES (?, ?, ?, ?, ?)"""
			
			for username in user_list:
				conn.execute(insertUserSQL, (str(username), "-1", "-1", "-1", "-1"))
		
		conn.commit()
		conn.close()
	except:
		print "Database read/write error ", sys.exc_info(), "line ", sys.exc_info()[2].tb_lineno

def usersDBIsEmpty():
	listTuples = getAllUsers()
	return len(listTuples)==0

def initOnlineUsersDB():
	try:
		conn = sqlite3.connect("database.db")
		
		usersSQL = """CREATE TABLE IF NOT EXISTS onlineUsers(
						id 			integer PRIMARY KEY,
						username	text	NOT NULL,
						ip			text	NOT NULL,
						location	text	NOT NULL,
						lastLogin	text	NOT NULL,
						port		text	NOT NULL)"""
		conn.execute(usersSQL)
		
		conn.commit()
		conn.close()
	except:
		print "Database read/write error ", sys.exc_info(), "line ", sys.exc_info()[2].tb_lineno
	
def initMsgsDB():
	try:
		conn = sqlite3.connect("database.db")
		
		msgsSQL = """CREATE TABLE IF NOT EXISTS messages(
			id 			integer PRIMARY KEY,
			sender		text	NOT NULL,
			receiver	text	NOT NULL,
			message		text	NOT NULL,
			timestamp	text	NOT NULL,
			hash		text,
			read		text,
			acknowledged	text,
			isFile		text)"""
		conn.execute(msgsSQL)
		
		conn.commit()
		conn.close()
	except:
		print "Database read/write error ", sys.exc_info(), "line ", sys.exc_info()[2].tb_lineno
	
def initFilesDB():
	try:
		conn = sqlite3.connect("database.db")
		
		filesSQL = """CREATE TABLE IF NOT EXISTS files(
			id 			integer PRIMARY KEY,
			sender		text	NOT NULL,
			receiver	text	NOT NULL,
			file		text,
			filename	text	NOT NULL,
			content_type	text	NOT NULL,
			timestamp	text	NOT NULL,
			hash		text,
			read		text)"""
		conn.execute(filesSQL)
		
		conn.commit()
		conn.close()
	except:
		print "Database read/write error ", sys.exc_info(), "line ", sys.exc_info()[2].tb_lineno
	
def initProfilesDB(user_list):
	try:
		conn = sqlite3.connect("database.db")
		
		profilesSQL = """CREATE TABLE IF NOT EXISTS profiles(
			id 			integer PRIMARY KEY,
			username	text	UNIQUE,
			lastUpdated	text	NOT NULL,
			fullname	text	NOT NULL,
			position	text	NOT NULL,
			description	text	NOT NULL,
			location	text	NOT NULL,
			picture		text	NOT NULL)"""
		conn.execute(profilesSQL)
		
		if profilesDBIsEmpty():
			insertProfileSQL = """INSERT OR REPLACE INTO profiles (username, lastUpdated, fullname, position, description, location, picture) VALUES (?, ?, ?, ?, ?, ?, ?)"""
			
			for username in user_list:
				conn.execute(insertProfileSQL, (str(username), "0", "-1", "-1", "-1", "-1", "-1"))
		
		conn.commit()
		conn.close()
	except:
		print "Database read/write error ", sys.exc_info(), "line ", sys.exc_info()[2].tb_lineno

def profilesDBIsEmpty():
	listTuples = getAllProfiles()
	return len(listTuples)==0

def initLoginDB():
	try:
		conn = sqlite3.connect("database.db")
		
		loginSQL = """CREATE TABLE IF NOT EXISTS logins(
						id 			integer PRIMARY KEY,
						username	text	NOT NULL,
						hashedPass	text	NOT NULL,
						pubKey			text,
						privKey			text)"""
		conn.execute(loginSQL)
		
		conn.commit()
		conn.close()
	except:
		print "Database read/write error ", sys.exc_info(), "line ", sys.exc_info()[2].tb_lineno

def initRequestsDB():
	try:
		conn = sqlite3.connect("database.db")
		
		reqSQL = """CREATE TABLE IF NOT EXISTS requests(
						id 			integer PRIMARY KEY,
						username	text	NOT NULL,
						timestamp	text	NOT NULL,
						ip			text)"""
		conn.execute(reqSQL)
		
		conn.commit()
		conn.close()
	except:
		print "Database read/write error ", sys.exc_info(), "line ", sys.exc_info()[2].tb_lineno
	
def vacuumDatabase():			# run this to defragment the database
	try:
		conn = sqlite3.connect("database.db")
		conn.execute("VACUUM")
		conn.close()
	except:
		print "Database vacuum error ", sys.exc_info(), "line ", sys.exc_info()[2].tb_lineno

def updateUsersDB(dict_data):
	try:
		conn = sqlite3.connect("database.db")
		updateUserSQL = "UPDATE users SET ip = ?, location = ?, lastLogin = ?, port = ? WHERE username = ?"
		
		for user in dict_data.values():
			conn.execute(updateUserSQL, (user["ip"], user["location"], user["lastLogin"], user["port"], user["username"]))
		
		conn.commit()
		conn.close()
	except:
		print "Database read/write error ", sys.exc_info(), "line ", sys.exc_info()[2].tb_lineno

def findUser(username):
	try:
		targetTuple = None
		listTuples = getAllUsers()
		
		for row in listTuples:
			if str(row[0]) == str(username):
				targetTuple = row
		
		return targetTuple
	except:
		print "Database read/write error ", sys.exc_info(), "line ", sys.exc_info()[2].tb_lineno
		
def getAllUsers():
	try:
		conn = sqlite3.connect("database.db")
		curs = conn.execute("SELECT username, ip, location, lastLogin, port FROM users")
		
		listTuples = curs.fetchall()
		
		conn.commit()
		conn.close()
		
		return listTuples
	except:
		print "Database read/write error ", sys.exc_info(), "line ", sys.exc_info()[2].tb_lineno

def updateOnlineUsersDB(dict_data):
	try:
		deleteAllOnlineUsers()		# delete all onlineUsers from the database
		
		conn = sqlite3.connect("database.db")
		insertUserSQL = "INSERT INTO onlineUsers (username, ip, location, lastLogin, port) VALUES (?, ?, ?, ?, ?)"
		
		for user in dict_data.values():			# re-add all users that are in the dict passed in
			conn.execute(insertUserSQL, (user["username"], user["ip"], user["location"], user["lastLogin"], user["port"]))
		
		conn.commit()
		conn.close()
	except:
		print "Database read/write error ", sys.exc_info(), "line ", sys.exc_info()[2].tb_lineno

def deleteAllOnlineUsers():
	try:
		conn = sqlite3.connect("database.db")
		deleteOnlineUsersSQL = "DELETE FROM onlineUsers "

		conn.execute(deleteOnlineUsersSQL)
		
		conn.commit()
		conn.close()
	except:
		print "Database read/write error ", sys.exc_info(), "line ", sys.exc_info()[2].tb_lineno
	
def findOnlineUser(username):
	try:
		targetTuple = None
		listTuples = getAllOnlineUser()
		
		for row in listTuples:
			if row[0] == username:
				targetTuple = row
		
		return targetTuple
	except:
		print "Database read/write error ", sys.exc_info(), "line ", sys.exc_info()[2].tb_lineno

def getAllOnlineUser():
	try:
		conn = sqlite3.connect("database.db")
		curs = conn.execute("SELECT username, ip, location, lastLogin, port FROM onlineUsers")
		
		listTuples = curs.fetchall()
		
		conn.commit()
		conn.close()
		
		return listTuples
	except:
		print "Database read/write error ", sys.exc_info(), "line ", sys.exc_info()[2].tb_lineno

def updateMsgsDB(dict_data):
	try:
		dict_data["hash"] = hashMsgData(dict_data["message"], dict_data["stamp"], dict_data["sender"])
		tuple_data = (dict_data["sender"], dict_data["destination"], dict_data["message"], dict_data["stamp"], dict_data["hash"])

		conn = sqlite3.connect("database.db")
		insertMsgSQL = """INSERT INTO messages (sender, receiver, message, timestamp, hash) VALUES (?, ?, ?, ?, ?)"""
		
		if not msgExists(dict_data["hash"]):			#duplicate checking
			conn.execute(insertMsgSQL, tuple_data)
		
		conn.commit()
		conn.close()
	except:
		print "Database read/write error ", sys.exc_info(), "line ", sys.exc_info()[2].tb_lineno
	
def hashMsgData(data, timestamp, username=None):
	dataString = (data.encode("ascii", "ignore")+str(timestamp)+str(username))
	hashedData = sha256(dataString.encode()).hexdigest()
	return hashedData

def msgExists(hash):		# use the message hash to check if a message exists
	try:
		messageFound = False
		listTuples = getAllMsgs()
		
		for row in listTuples:
			if row[5] == hash:
				messageFound = True
		
		return messageFound
	except:
		print "Database read/write error ", sys.exc_info(), "line ", sys.exc_info()[2].tb_lineno
	
def deleteAMsg(hash):
	try:
		conn = sqlite3.connect("database.db")
		deleteMessagesSQL = "DELETE FROM messages WHERE hash=?"

		conn.execute(deleteMessagesSQL, (hash,))
		
		conn.commit()
		conn.close()
	except:
		print "Database read/write error ", sys.exc_info(), "line ", sys.exc_info()[2].tb_lineno
	
def deleteAllMsgs():
	try:
		conn = sqlite3.connect("database.db")
		deleteMessagesSQL = "DELETE FROM messages "

		conn.execute(deleteMessagesSQL)
		
		conn.commit()
		conn.close()
	except:
		print "Database read/write error ", sys.exc_info(), "line ", sys.exc_info()[2].tb_lineno
	
def getMsgByID(ID):
	try:
		listTuples = getAllMsgs()
		
		targetTuple = None
		
		for row in listTuples:
			if str(row[0]) == str(ID):
				targetTuple = row
		
		return targetTuple
	except:
		print "Database read/write error ", sys.exc_info(), "line ", sys.exc_info()[2].tb_lineno
	
def deleteByID(ID):
	try:
		conn = sqlite3.connect("database.db")
		deleteMessagesSQL = "DELETE FROM messages WHERE id=?"

		conn.execute(deleteMessagesSQL, (ID,))
		
		conn.commit()
		conn.close()
	except:
		print "Database read/write error ", sys.exc_info(), "line ", sys.exc_info()[2].tb_lineno
	
def getAllMsgs():
	try:
		conn = sqlite3.connect("database.db")
		curs = conn.execute("SELECT * FROM messages")
		
		listTuples = curs.fetchall()
		
		conn.commit()
		conn.close()
		
		return listTuples
	except:
		print "Database read/write error ", sys.exc_info(), "line ", sys.exc_info()[2].tb_lineno
	
def setMsgRead(read, hash):
	try:
		conn = sqlite3.connect("database.db")
		updateMsgSQL = """UPDATE messages SET read=? WHERE hash=?"""
		
		conn.execute(updateMsgSQL, (read, hash))
		
		conn.commit()
		conn.close()
	except:
		print "Database read/write error ", sys.exc_info(), "line ", sys.exc_info()[2].tb_lineno
	
def setMsgAcknowledged(acknowledged, hash):
	try:
		conn = sqlite3.connect("database.db")
		updateMsgSQL = """UPDATE messages SET acknowledged=? WHERE hash=?"""
		
		conn.execute(updateMsgSQL, (acknowledged, hash))
		
		conn.commit()
		conn.close()
	except:
		print "Database read/write error ", sys.exc_info(), "line ", sys.exc_info()[2].tb_lineno
		
def getLatestMsg():
	try:
		targetTuple = None
		listTuples = getAllMsgs()
		
		for row in listTuples:
			targetTuple = row
		
		return targetTuple
	except:
		print "Database read/write error ", sys.exc_info(), "line ", sys.exc_info()[2].tb_lineno

def updateProfilesDB(tup):		# update or insert user profile (assumes timestamp has been checked and data should be updated)
	try:
		username = tup[0]
		tuple_data_insert = tup
		tuple_data_update = (tup[1], tup[2], tup[3], tup[4], tup[5], tup[6], tup[0])

		conn = sqlite3.connect("database.db")
		insertProfileSQL = """INSERT INTO profiles (username, lastUpdated, fullname, position, description, location, picture) VALUES (?, ?, ?, ?, ?, ?, ?)"""
		updateProfileSQL = """UPDATE profiles SET lastUpdated=?, fullname=?, position=?, description=?, location=?, picture=? WHERE username=?"""

		dbData = findProfile(username)
		if dbData == None:
			conn.execute(insertProfileSQL, tuple_data_insert)
		else:
			conn.execute(updateProfileSQL, tuple_data_update)
		
		conn.commit()
		conn.close()
	except:
		print "Database read/write error ", sys.exc_info(), "line ", sys.exc_info()[2].tb_lineno
	
def findProfile(username):
	try:
		targetDict = None
		listTuples = getAllProfiles()
		
		for row in listTuples:
			if row[1] == username:
				targetDict = profileTupleToDict(row)
		
		return targetDict
	except:
		print "Database read/write error ", sys.exc_info(), "line ", sys.exc_info()[2].tb_lineno
	
def getAllProfiles():
	try:
		conn = sqlite3.connect("database.db")
		curs = conn.execute("SELECT * FROM profiles")
		
		listTuples = curs.fetchall()
		
		conn.commit()
		conn.close()
		
		return listTuples
	except:
		print "Database read/write error ", sys.exc_info(), "line ", sys.exc_info()[2].tb_lineno
	
def getAllProfilesDict():
	try:
		list_dicts = []
		listTuples = getAllProfiles()

		for row in listTuples:
			list_dicts.append( profileTupleToDict(row) )
		
		return list_dicts
	except:
		print "Database read/write error ", sys.exc_info(), "line ", sys.exc_info()[2].tb_lineno

def profileTupleToDict(row):
	dict = {"username": row[1], "lastUpdated": float(row[2]), "fullname": row[3], "position": row[4], "description": row[5], "location": row[6], "picture": row[7]}
	return dict

def updateFilesDB(dict_data):
	try:
		tuple_data = (dict_data["sender"], dict_data["destination"], "(not stored)", dict_data["filename"], dict_data["content_type"], dict_data["stamp"])

		conn = sqlite3.connect("database.db")
		insertFileSQL = """INSERT INTO files (sender, receiver, file, filename, content_type, timestamp) VALUES (?, ?, ?, ?, ?, ?)"""
		
		conn.execute(insertFileSQL, tuple_data)

		conn.commit()
		conn.close()
	except:
		print "Database read/write error ", sys.exc_info(), "line ", sys.exc_info()[2].tb_lineno
	
def getAllFiles():
	try:
		conn = sqlite3.connect("database.db")
		curs = conn.execute("SELECT * FROM files")
		
		listTuples = curs.fetchall()
		
		conn.commit()
		conn.close()
		
		return listTuples
	except:
		print "Database read/write error ", sys.exc_info(), "line ", sys.exc_info()[2].tb_lineno
	
def deleteAllFiles():
	try:
		conn = sqlite3.connect("database.db")
		deleteFilesSQL = "DELETE FROM files "

		conn.execute(deleteFilesSQL)
		
		conn.commit()
		conn.close()
	except:
		print "Database read/write error ", sys.exc_info(), "line ", sys.exc_info()[2].tb_lineno

def addLogin(username, hashedPass, pubKey=None, privKey=None):
	try:
		conn = sqlite3.connect("database.db")
		insertLoginSQL = """INSERT INTO logins (username, hashedPass, pubKey, privKey) VALUES (?, ?, ?, ?)"""
		
		conn.execute(insertLoginSQL, (username, hashedPass, pubKey, privKey))
		
		conn.commit()
		conn.close()
	except:
		print "Database read/write error ", sys.exc_info(), "line ", sys.exc_info()[2].tb_lineno

def getLoginData(username=""):
	try:
		targetTuple = None
		listTuples = getAllLoginData()
		
		for row in listTuples:
			if username=="" or row[1]==username:
				targetTuple = row
		
		return targetTuple
	except:
		print "Database read/write error ", sys.exc_info(), "line ", sys.exc_info()[2].tb_lineno

def getAllLoginData():
	try:
		conn = sqlite3.connect("database.db")		
		curs = conn.execute("SELECT * FROM logins")
		
		listTuples = curs.fetchall()
		
		conn.commit()
		conn.close()
		
		return listTuples
	except:
		print "Database read/write error ", sys.exc_info(), "line ", sys.exc_info()[2].tb_lineno

def deleteLogins():
	try:
		conn = sqlite3.connect("database.db")
		deleteLoginSQL = "DELETE FROM logins"# where username=?"

		conn.execute(deleteLoginSQL)
		
		conn.commit()
		conn.close()
	except:
		print "Database read/write error ", sys.exc_info(), "line ", sys.exc_info()[2].tb_lineno

def deleteUesrLogin(username):
	try:
		conn = sqlite3.connect("database.db")
		deleteLoginSQL = "DELETE FROM logins where username=?"

		conn.execute(deleteLoginSQL, (username,))
		
		conn.commit()
		conn.close()
	except:
		print "Database read/write error ", sys.exc_info(), "line ", sys.exc_info()[2].tb_lineno

def hasLogins():
	initLoginDB()
	return (getLoginData() != None)
	
def getAllLogins():
	try:
		conn = sqlite3.connect("database.db")
		curs = conn.execute("SELECT * FROM logins")
		
		listTuples = curs.fetchall()
		
		conn.commit()
		conn.close()
		
		return listTuples
	except:
		print "Database read/write error ", sys.exc_info(), "line ", sys.exc_info()[2].tb_lineno

def addRequest(username, timestamp):
	try:
		conn = sqlite3.connect("database.db")
		insertReqSQL = """INSERT INTO requests (username, timestamp) VALUES (?, ?)"""
		
		conn.execute(insertReqSQL, (username, timestamp))
		
		conn.commit()
		conn.close()
	except:
		print "Database read/write error ", sys.exc_info(), "line ", sys.exc_info()[2].tb_lineno

def deleteRequests():
	try:
		conn = sqlite3.connect("database.db")
		deleteReqsSQL = "DELETE FROM requests"

		conn.execute(deleteReqsSQL)
		
		conn.commit()
		conn.close()
	except:
		print "Database read/write error ", sys.exc_info(), "line ", sys.exc_info()[2].tb_lineno

def deleteARequest(timestamp):
	try:
		conn = sqlite3.connect("database.db")
		deleteReqSQL = "DELETE FROM requests where timestamp=?"

		conn.execute(deleteReqSQL, (timestamp,))
		
		conn.commit()
		conn.close()
	except:
		print "Database read/write error ", sys.exc_info(), "line ", sys.exc_info()[2].tb_lineno
	
def deleteRequestRange(beforeTimestamp):
	try:
		conn = sqlite3.connect("database.db")
		deleteReqSQL = "DELETE FROM requests where timestamp<=?"

		conn.execute(deleteReqSQL, (beforeTimestamp,))
		
		conn.commit()
		conn.close()
	except:
		print "Database read/write error ", sys.exc_info(), "line ", sys.exc_info()[2].tb_lineno
	
def getAllRequests():
	try:
		conn = sqlite3.connect("database.db")
		curs = conn.execute("SELECT * FROM requests")
		
		listTuples = curs.fetchall()
		
		conn.commit()
		conn.close()
		
		return listTuples
	except:
		print "Database read/write error ", sys.exc_info(), "line ", sys.exc_info()[2].tb_lineno

def getUserRequests(username, sinceTimestamp):		# get all requests from the user, where the time is after the specified epoch
	try:
		targetlistTuples = []
		listTuples = getAllRequests()

		for row in listTuples:
			if row[1]==username:
				if int(float(row[2])) > int(float(sinceTimestamp)):
					targetlistTuples.append(row)
		
		return targetlistTuples
	except:
		print "Database read/write error ", sys.exc_info(), "line ", sys.exc_info()[2].tb_lineno
