import binascii
from Crypto.Cipher import XOR
from Crypto.Cipher import AES
from Crypto.PublicKey import RSA
from Crypto import Random

loginServerAESKey = "150ecd12d550d05ad83f18328e536f53"
p2pAESKey = "41fb5b5ae4d57c5ee528adb078ac3b2e"
AESBlockSize = 16

# Encryption

def AESEncryptDict(dict, loginServ=True):		# encrypt a dictionary's values with AES-256 (for login server / p2p)
	for key,val in dict.items():
		if loginServ:
			if key == "enc":			# do not encrypt the enc value
				continue
			dict[key] = AESEncrypt(str(val), loginServerAESKey)
		else:
			if key == "enc":
				continue
			dict[key] = AESEncrypt(str(val), p2pAESKey)

def AESDecryptDict(dict, loginServ=True):		# decrypt a dictionary's values with AES-256 (for login server / p2p)
	for key,val in dict.items():
		if loginServ:
			if key == "enc":
				continue
			dict[key] = AESDecrypt(str(val), loginServerAESKey)
		else:
			if key == "enc":
				continue
			dict[key] = AESDecrypt(str(val), p2pAESKey)
	
# XOR Encryption
	
def xorEncrypt(plainText):		# encrypt plain text with the default xor key
	cipher = XOR.new("10010110")
	cipherText = cipher.encrypt(plainText)
	return binascii.hexlify(cipherText)

def xorDecrypt(cipherText):		# decrypt data using same xor key (symmetric)
	cipherText = binascii.unhexlify(cipherText)
	cipher = XOR.new("10010110")
	plainText = cipher.decrypt(cipherText)
	return plainText

# AES-256 Encryption

def AESEncrypt(plainText, key):			# adapted from https://stackoverflow.com/questions/12524994/encrypt-decrypt-using-pycrypto-aes-256
	plainText = padData(plainText)
	iv = Random.new().read(AESBlockSize)			# generate a 16-byte iv
	cipher = AES.new(key, AES.MODE_CBC, iv)
	cipherText = cipher.encrypt(iv+plainText)		# encrypt with the first 16 bytes as the iv
	return hex(cipherText)

def AESDecrypt(cipherText, key):		# adapted from the 'Login Server Protocol' example
	cipherText = unhex(cipherText)		
	iv = cipherText[:AESBlockSize]					# iv is first 16 bytes
	cipher = AES.new(key, AES.MODE_CBC, iv)
	plainText = cipher.decrypt(cipherText[AESBlockSize:])		# decrypt the non-iv part
	return unpadData(plainText)

def padData(txt):			# pad data so that it's in blocks of 16
	lenToPad = AESBlockSize - (len(txt) % AESBlockSize)
	txt += chr(lenToPad)*(lenToPad)			# pad with the length padded for ease of unpadding
	return txt

def unpadData(txt):			# unpad data back to original length
	lenToUnpad = txt[-1]
	txt = txt[:-lenToUnpad]					# remove the last lenToUnpad bytes
	return txt

# RSA-1024 Encryption

def RSAGenerateKeys(size_bits=1024):		# generates public-private key pairs (1024 bits by default, DER mode); returned as a dict (hex strings)
	newKey = RSA.generate(size_bits)
	publicKey = newKey.publickey().exportKey("DER")
	privateKey = newKey.exportKey("DER")
	# all keys are stored as hex strings for ease of reading/writing/sending to database/nodes
	keys_dict = {"public": hex(publicKey), "private": hex(privateKey)}
	return keys_dict
	
def RSAEncrypt(plainText, pubKey):			# encrypt some data with someone's public key
	pubKeyObj = getKeyObj(pubKey)
	if len(plainText) >= 128:				# checks that message size is less than 128 chars
		plainText = plainText[:128]			# only takes in 128 character maximum
	cipherText = pubKeyObj.encrypt(plainText, None)[0]
	return hex(cipherText)

def RSADecrypt(cipherText, privKey):		# decrypt encrypted data using your private key
	privKeyObj = getKeyObj(privKey)
	cipherText = unhex(cipherText)
	plainText = privKeyObj.decrypt(cipherText)
	return plainText

def getKeyObj(keyStr):
	return RSA.importKey(unhex(keyStr))

def hex(data):
	return binascii.hexlify(data)
	
def unhex(data):
	return binascii.unhexlify(data)

def checkRSAKeys(pubKey, privKey):				# function for verifying that the encrypt and decrypt works
	test_data = "This is a test message. "*10
	print test_data
	enc_data = RSAEncrypt(test_data, pubKey)
	print enc_data
	dec_data = RSADecrypt(enc_data, privKey)
	print dec_data
	
	return test_data == dec_data
